/*
 * Copyright 2019 EPAM Systems
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const STATS_TB_TOTAL = 'statistics$defects$test_bug$total';
export const STATS_MD_TOTAL = 'statistics$defects$minor_defect$total';
export const STATS_PB_TOTAL = 'statistics$defects$product_bug$total';
export const STATS_SI_TOTAL = 'statistics$defects$system_issue$total';
export const STATS_TI_TOTAL = 'statistics$defects$to_investigate$total';

export const STATS_TOTAL = 'statistics$executions$total';
export const STATS_FAILED = 'statistics$executions$failed';
export const STATS_PASSED = 'statistics$executions$passed';
export const STATS_UNTESTED = 'statistics$executions$untested';
export const STATS_RUNNING = 'statistics$executions$running';
export const STATS_SKIPPED = 'statistics$executions$skipped';
