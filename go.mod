module github.com/reportportal/service-ui

go 1.12

require (
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/reportportal/commons-go/v5 v5.0.7
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/unrolled/secure v0.0.0-20170904132641-19a872c81779
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
